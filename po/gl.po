# Galician translation of libgweather.
# Copyright (C) 1999-2002 Jesús Bravo Álvarez
# Proxecto Trasno - Adaptación do software libre á lingua galega:  Se desexas
# colaborar connosco, podes atopar máis información en http://www.trasno.net
# Jesús Bravo Álvarez <suso@trasno.net>, 1999-2002.
# Ignacio Casal Quinteiro <nacho.resa@gmail.com>, 2005, 2006.
# Ignacio Casal Quinteiro <icq@svn.gnome.org>, 2008.
# Mancomún - Centro de Referencia e Servizos de Software Libre <g11n@mancomun.org>, 2009.
# Antón Méixome <meixome@mancomun.org>, 2009.
# Fran Diéguez <frandieguez@gnome.org>, 2009, 2010, 2011, 2012.
# Leandro Regueiro <leandro.regueiro@gmail.com>, 2012.
# Fran Dieguez <frandieguez@gnome.org>, 2012, 2013, 2014, 2015, 2016, 2017, 2018.
msgid ""
msgstr ""
"Project-Id-Version: libgweather-master-po-gl-57986\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/GNOME/libgweather/issues\n"
"POT-Creation-Date: 2019-10-17 12:35+0000\n"
"PO-Revision-Date: 2019-12-27 13:17+0100\n"
"Last-Translator: Fran Diéguez <frandieguez@gnome.org>\n"
"Language-Team: Proxecto Trasno <proxecto@trasno.gal>\n"
"Language: gl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 2.2.4\n"
"X-Project-Style: gnome\n"

#. Recurse, adding the ADM1 name to the country name
#. Translators: this is the name of a location followed by a region, for example:
#. * 'London, United Kingdom'
#. * You shouldn't need to translate this string unless the language has a different comma.
#.
#. <location> with no parent <city>
#. Translators: this is the name of a location followed by a region, for example:
#. * 'London, United Kingdom'
#. * You shouldn't need to translate this string unless the language has a different comma.
#.
#: libgweather/gweather-location-entry.c:536
#: libgweather/gweather-location-entry.c:564
#, c-format
msgid "%s, %s"
msgstr "%s, %s"

#: libgweather/gweather-location-entry.c:824
msgid "Loading…"
msgstr "Cargando…"

#: libgweather/gweather-timezone.c:343
msgid "Greenwich Mean Time"
msgstr "Meridiano de hora Greenwich"

#: libgweather/gweather-timezone-menu.c:274
msgctxt "timezone"
msgid "Unknown"
msgstr "Descoñecida"

#: libgweather/gweather-weather.c:119
msgid "variable"
msgstr "variábel"

#: libgweather/gweather-weather.c:120
msgid "north"
msgstr "norte"

#: libgweather/gweather-weather.c:120
msgid "north — northeast"
msgstr "norte — nordeste"

#: libgweather/gweather-weather.c:120
msgid "northeast"
msgstr "nordeste"

#: libgweather/gweather-weather.c:120
msgid "east — northeast"
msgstr "leste — sueste"

#: libgweather/gweather-weather.c:121
msgid "east"
msgstr "nordeste"

#: libgweather/gweather-weather.c:121
msgid "east — southeast"
msgstr "leste — sueste"

#: libgweather/gweather-weather.c:121
msgid "southeast"
msgstr "sueste"

#: libgweather/gweather-weather.c:121
msgid "south — southeast"
msgstr "sur — sueste"

#: libgweather/gweather-weather.c:122
msgid "south"
msgstr "sur"

#: libgweather/gweather-weather.c:122
msgid "south — southwest"
msgstr "sur — suroeste"

#: libgweather/gweather-weather.c:122
msgid "southwest"
msgstr "suroeste"

#: libgweather/gweather-weather.c:122
msgid "west — southwest"
msgstr "oeste — suroeste"

#: libgweather/gweather-weather.c:123
msgid "west"
msgstr "suroeste"

#: libgweather/gweather-weather.c:123
msgid "west — northwest"
msgstr "oeste — noroeste"

#: libgweather/gweather-weather.c:123
msgid "northwest"
msgstr "noroeste"

#: libgweather/gweather-weather.c:123
msgid "north — northwest"
msgstr "norte — noroeste"

#: libgweather/gweather-weather.c:127
msgid "Variable"
msgstr "Variábel"

#: libgweather/gweather-weather.c:128
msgid "North"
msgstr "Norte"

#: libgweather/gweather-weather.c:128
msgid "North — Northeast"
msgstr "Norte — Nordeste"

#: libgweather/gweather-weather.c:128
msgid "Northeast"
msgstr "Nordeste"

#: libgweather/gweather-weather.c:128
msgid "East — Northeast"
msgstr "Leste — nordeste"

#: libgweather/gweather-weather.c:129
msgid "East"
msgstr "Leste"

#: libgweather/gweather-weather.c:129
msgid "East — Southeast"
msgstr "Leste — Sueste"

#: libgweather/gweather-weather.c:129
msgid "Southeast"
msgstr "Sueste"

#: libgweather/gweather-weather.c:129
msgid "South — Southeast"
msgstr "Sur — Sueste"

#: libgweather/gweather-weather.c:130
msgid "South"
msgstr "Sur"

#: libgweather/gweather-weather.c:130
msgid "South — Southwest"
msgstr "Sur — Suroeste"

#: libgweather/gweather-weather.c:130
msgid "Southwest"
msgstr "Suroeste"

#: libgweather/gweather-weather.c:130
msgid "West — Southwest"
msgstr "Oeste — Suroeste"

#: libgweather/gweather-weather.c:131
msgid "West"
msgstr "Oeste"

#: libgweather/gweather-weather.c:131
msgid "West — Northwest"
msgstr "Oeste — Noroeste"

#: libgweather/gweather-weather.c:131
msgid "Northwest"
msgstr "Noroeste"

#: libgweather/gweather-weather.c:131
msgid "North — Northwest"
msgstr "Norte — noroeste"

#: libgweather/gweather-weather.c:141
msgctxt "wind direction"
msgid "Invalid"
msgstr "Non válido"

#: libgweather/gweather-weather.c:142
msgctxt "wind direction"
msgid "invalid"
msgstr "non válido"

#: libgweather/gweather-weather.c:155
msgid "clear sky"
msgstr "ceo limpo"

#: libgweather/gweather-weather.c:156
msgid "broken clouds"
msgstr "anubrado"

#: libgweather/gweather-weather.c:157
msgid "scattered clouds"
msgstr "pouco anubrado"

#: libgweather/gweather-weather.c:158
msgid "few clouds"
msgstr "algunhas nubes"

#: libgweather/gweather-weather.c:159
msgid "overcast"
msgstr "moi anubrado"

#: libgweather/gweather-weather.c:163
msgid "Clear sky"
msgstr "Ceo limpo"

#: libgweather/gweather-weather.c:164
msgid "Broken clouds"
msgstr "Anubrado"

#: libgweather/gweather-weather.c:165
msgid "Scattered clouds"
msgstr "Pouco anubrado"

#: libgweather/gweather-weather.c:166
msgid "Few clouds"
msgstr "Algunhas nubes"

#: libgweather/gweather-weather.c:167
msgid "Overcast"
msgstr "Moi anubrado"

#: libgweather/gweather-weather.c:183 libgweather/gweather-weather.c:297
msgctxt "sky conditions"
msgid "Invalid"
msgstr "Non válido"

#: libgweather/gweather-weather.c:184 libgweather/gweather-weather.c:298
msgctxt "sky conditions"
msgid "invalid"
msgstr "non válido"

#. TRANSLATOR: If you want to know what "blowing" "shallow" "partial"
#. * etc means, you can go to http://www.weather.com/glossary/ and
#. * http://www.crh.noaa.gov/arx/wx.tbl.php
#. NONE
#: libgweather/gweather-weather.c:217 libgweather/gweather-weather.c:219
msgid "thunderstorm"
msgstr "treboada"

#. DRIZZLE
#: libgweather/gweather-weather.c:218
msgid "drizzle"
msgstr "chuvisca"

#: libgweather/gweather-weather.c:218
msgid "light drizzle"
msgstr "chuvisca lixeira"

#: libgweather/gweather-weather.c:218
msgid "moderate drizzle"
msgstr "chuvisca moderada"

#: libgweather/gweather-weather.c:218
msgid "heavy drizzle"
msgstr "chuvisca forte"

#: libgweather/gweather-weather.c:218
msgid "freezing drizzle"
msgstr "chuvisca xeada"

#. RAIN
#: libgweather/gweather-weather.c:219
msgid "rain"
msgstr "chuvia"

#: libgweather/gweather-weather.c:219
msgid "light rain"
msgstr "chuvia lixeira"

#: libgweather/gweather-weather.c:219
msgid "moderate rain"
msgstr "chuvia moderada"

#: libgweather/gweather-weather.c:219
msgid "heavy rain"
msgstr "chuvia forte"

#: libgweather/gweather-weather.c:219
msgid "rain showers"
msgstr "chuvia abundante"

#: libgweather/gweather-weather.c:219
msgid "freezing rain"
msgstr "chuvia xeada"

#. SNOW
#: libgweather/gweather-weather.c:220
msgid "snow"
msgstr "neve"

#: libgweather/gweather-weather.c:220
msgid "light snow"
msgstr "neve lixeira"

#: libgweather/gweather-weather.c:220
msgid "moderate snow"
msgstr "neve moderada"

#: libgweather/gweather-weather.c:220
msgid "heavy snow"
msgstr "neve forte"

#: libgweather/gweather-weather.c:220
msgid "snowstorm"
msgstr "treboada de neve"

#: libgweather/gweather-weather.c:220
msgid "blowing snowfall"
msgstr "nevada con vento"

#: libgweather/gweather-weather.c:220
msgid "snow showers"
msgstr "neve abundante"

#: libgweather/gweather-weather.c:220
msgid "drifting snow"
msgstr "neve flutuante"

#. SNOW_GRAINS
#: libgweather/gweather-weather.c:221
msgid "snow grains"
msgstr "folerpas de neve"

#: libgweather/gweather-weather.c:221
msgid "light snow grains"
msgstr "folerpas de neve lixeiras"

#: libgweather/gweather-weather.c:221
msgid "moderate snow grains"
msgstr "folerpas de neve moderadas"

#: libgweather/gweather-weather.c:221
msgid "heavy snow grains"
msgstr "folerpas de neve fortes"

#. ICE_CRYSTALS
#: libgweather/gweather-weather.c:222
msgid "ice crystals"
msgstr "cristais de xeo"

#. ICE_PELLETS
#: libgweather/gweather-weather.c:223
msgid "sleet"
msgstr "auga neve"

#: libgweather/gweather-weather.c:223
msgid "little sleet"
msgstr "auga neve liviana"

#: libgweather/gweather-weather.c:223
msgid "moderate sleet"
msgstr "auga neve moderada"

#: libgweather/gweather-weather.c:223
msgid "heavy sleet"
msgstr "auga neve forte"

#: libgweather/gweather-weather.c:223
msgid "sleet storm"
msgstr "tormenta de auga neve"

#: libgweather/gweather-weather.c:223
msgid "showers of sleet"
msgstr "xaraivas de auga neve"

#. HAIL
#: libgweather/gweather-weather.c:224
msgid "hail"
msgstr "pedrazo"

#: libgweather/gweather-weather.c:224
msgid "hailstorm"
msgstr "treboada de pedrazo"

#: libgweather/gweather-weather.c:224
msgid "hail showers"
msgstr "fortes precipitacións de pedrazo"

#. SMALL_HAIL
#: libgweather/gweather-weather.c:225
msgid "small hail"
msgstr "pedrazo lixeiro"

#: libgweather/gweather-weather.c:225
msgid "small hailstorm"
msgstr "treboada de pedrazo lixeira"

#: libgweather/gweather-weather.c:225
msgid "showers of small hail"
msgstr "precipitacións de pedrazo lixeiro"

#. PRECIPITATION
#: libgweather/gweather-weather.c:226
msgid "unknown precipitation"
msgstr "precipitación descoñecida"

#. MIST
#: libgweather/gweather-weather.c:227
msgid "mist"
msgstr "brétema"

#. FOG
#: libgweather/gweather-weather.c:228
msgid "fog"
msgstr "néboa"

#: libgweather/gweather-weather.c:228
msgid "fog in the vicinity"
msgstr "néboa nas proximidades"

#: libgweather/gweather-weather.c:228
msgid "shallow fog"
msgstr "néboa pouco intensa"

#: libgweather/gweather-weather.c:228
msgid "patches of fog"
msgstr "parrumada"

#: libgweather/gweather-weather.c:228
msgid "partial fog"
msgstr "néboa parcial"

#: libgweather/gweather-weather.c:228
msgid "freezing fog"
msgstr "néboa xeada"

#. SMOKE
#: libgweather/gweather-weather.c:229
msgid "smoke"
msgstr "fume"

#. VOLCANIC_ASH
#: libgweather/gweather-weather.c:230
msgid "volcanic ash"
msgstr "cinza volcánica"

#. SAND
#: libgweather/gweather-weather.c:231
msgid "sand"
msgstr "area"

#: libgweather/gweather-weather.c:231
msgid "blowing sand"
msgstr "area con vento"

#: libgweather/gweather-weather.c:231
msgid "drifting sand"
msgstr "area flutuante"

#. HAZE
#: libgweather/gweather-weather.c:232
msgid "haze"
msgstr "bruma"

#. SPRAY
#: libgweather/gweather-weather.c:233
msgid "blowing sprays"
msgstr "orballo con vento"

#. DUST
#: libgweather/gweather-weather.c:234
msgid "dust"
msgstr "po"

#: libgweather/gweather-weather.c:234
msgid "blowing dust"
msgstr "po con vento"

#: libgweather/gweather-weather.c:234
msgid "drifting dust"
msgstr "po flutuante"

#. SQUALL
#: libgweather/gweather-weather.c:235
msgid "squall"
msgstr "refachos de vento"

#. SANDSTORM
#: libgweather/gweather-weather.c:236
msgid "sandstorm"
msgstr "treboada de area"

#: libgweather/gweather-weather.c:236
msgid "sandstorm in the vicinity"
msgstr "treboada de area nas proximidades"

#: libgweather/gweather-weather.c:236
msgid "heavy sandstorm"
msgstr "treboada forte de area"

#. DUSTSTORM
#: libgweather/gweather-weather.c:237
msgid "duststorm"
msgstr "treboada de po"

#: libgweather/gweather-weather.c:237
msgid "duststorm in the vicinity"
msgstr "treboada de po nas proximidades"

#: libgweather/gweather-weather.c:237
msgid "heavy duststorm"
msgstr "treboada forte de po"

#. FUNNEL_CLOUD
#: libgweather/gweather-weather.c:238
msgid "funnel cloud"
msgstr "nube de funil"

#. TORNADO
#: libgweather/gweather-weather.c:239
msgid "tornado"
msgstr "tornado"

#. DUST_WHIRLS
#: libgweather/gweather-weather.c:240
msgid "dust whirls"
msgstr "remuíños de po"

#: libgweather/gweather-weather.c:240
msgid "dust whirls in the vicinity"
msgstr "remuíños de po nas proximidades"

#. TRANSLATOR: If you want to know what "blowing" "shallow" "partial"
#. * etc means, you can go to http://www.weather.com/glossary/ and
#. * http://www.crh.noaa.gov/arx/wx.tbl.php
#. NONE
#: libgweather/gweather-weather.c:253 libgweather/gweather-weather.c:255
msgid "Thunderstorm"
msgstr "Treboada"

#. DRIZZLE
#: libgweather/gweather-weather.c:254
msgid "Drizzle"
msgstr "Chuvisca"

#: libgweather/gweather-weather.c:254
msgid "Light drizzle"
msgstr "Chuvisca lixeira"

#: libgweather/gweather-weather.c:254
msgid "Moderate drizzle"
msgstr "Chuvisca moderada"

#: libgweather/gweather-weather.c:254
msgid "Heavy drizzle"
msgstr "Chuvisca forte"

#: libgweather/gweather-weather.c:254
msgid "Freezing drizzle"
msgstr "Chuvisca xeada"

#. RAIN
#: libgweather/gweather-weather.c:255
msgid "Rain"
msgstr "Chuvia"

#: libgweather/gweather-weather.c:255
msgid "Light rain"
msgstr "Chuvia lixeira"

#: libgweather/gweather-weather.c:255
msgid "Moderate rain"
msgstr "Chuvia moderada"

#: libgweather/gweather-weather.c:255
msgid "Heavy rain"
msgstr "Chuvia forte"

#: libgweather/gweather-weather.c:255
msgid "Rain showers"
msgstr "Chuvia abundante"

#: libgweather/gweather-weather.c:255
msgid "Freezing rain"
msgstr "Chuvia xeada"

#. SNOW
#: libgweather/gweather-weather.c:256
msgid "Snow"
msgstr "Neve"

#: libgweather/gweather-weather.c:256
msgid "Light snow"
msgstr "Neve lixeira"

#: libgweather/gweather-weather.c:256
msgid "Moderate snow"
msgstr "Neve moderada"

#: libgweather/gweather-weather.c:256
msgid "Heavy snow"
msgstr "Neve forte"

#: libgweather/gweather-weather.c:256
msgid "Snowstorm"
msgstr "Treboada de neve"

#: libgweather/gweather-weather.c:256
msgid "Blowing snowfall"
msgstr "Nevada con vento"

#: libgweather/gweather-weather.c:256
msgid "Snow showers"
msgstr "Neve abundante"

#: libgweather/gweather-weather.c:256
msgid "Drifting snow"
msgstr "Neve flutuante"

#. SNOW_GRAINS
#: libgweather/gweather-weather.c:257
msgid "Snow grains"
msgstr "Folerpas de neve"

#: libgweather/gweather-weather.c:257
msgid "Light snow grains"
msgstr "Folerpas de neve lixeiras"

#: libgweather/gweather-weather.c:257
msgid "Moderate snow grains"
msgstr "Folerpas de neve moderadas"

#: libgweather/gweather-weather.c:257
msgid "Heavy snow grains"
msgstr "Folerpas de neve fortes"

#. ICE_CRYSTALS
#: libgweather/gweather-weather.c:258
msgid "Ice crystals"
msgstr "Cristais de xeo"

#. ICE_PELLETS
#: libgweather/gweather-weather.c:259
msgid "Sleet"
msgstr "Auga neve"

#: libgweather/gweather-weather.c:259
msgid "Little sleet"
msgstr "Auga neve liviana"

#: libgweather/gweather-weather.c:259
msgid "Moderate sleet"
msgstr "Auga neve moderada"

#: libgweather/gweather-weather.c:259
msgid "Heavy sleet"
msgstr "Auga neve forte"

#: libgweather/gweather-weather.c:259
msgid "Sleet storm"
msgstr "Tormenta de auga neve"

#: libgweather/gweather-weather.c:259
msgid "Showers of sleet"
msgstr "Xaraivas de auga neve"

#. HAIL
#: libgweather/gweather-weather.c:260
msgid "Hail"
msgstr "Pedrazo"

#: libgweather/gweather-weather.c:260
msgid "Hailstorm"
msgstr "Treboada de pedrazo"

#: libgweather/gweather-weather.c:260
msgid "Hail showers"
msgstr "Fortes precipitacións de pedrazo"

#. SMALL_HAIL
#: libgweather/gweather-weather.c:261
msgid "Small hail"
msgstr "Pedrazo lixeiro"

#: libgweather/gweather-weather.c:261
msgid "Small hailstorm"
msgstr "Treboada de pedrazo lixeira"

#: libgweather/gweather-weather.c:261
msgid "Showers of small hail"
msgstr "Precipitacións de pedrazo lixeiro"

#. PRECIPITATION
#: libgweather/gweather-weather.c:262
msgid "Unknown precipitation"
msgstr "Precipitación descoñecida"

#. MIST
#: libgweather/gweather-weather.c:263
msgid "Mist"
msgstr "Brétema"

#. FOG
#: libgweather/gweather-weather.c:264
msgid "Fog"
msgstr "Néboa"

#: libgweather/gweather-weather.c:264
msgid "Fog in the vicinity"
msgstr "Néboa nas proximidades"

#: libgweather/gweather-weather.c:264
msgid "Shallow fog"
msgstr "Néboa pouco intensa"

#: libgweather/gweather-weather.c:264
msgid "Patches of fog"
msgstr "Parrumada"

#: libgweather/gweather-weather.c:264
msgid "Partial fog"
msgstr "Néboa parcial"

#: libgweather/gweather-weather.c:264
msgid "Freezing fog"
msgstr "Néboa xeada"

#. SMOKE
#: libgweather/gweather-weather.c:265
msgid "Smoke"
msgstr "Fume"

#. VOLCANIC_ASH
#: libgweather/gweather-weather.c:266
msgid "Volcanic ash"
msgstr "Cinza volcánica"

#. SAND
#: libgweather/gweather-weather.c:267
msgid "Sand"
msgstr "Area"

#: libgweather/gweather-weather.c:267
msgid "Blowing sand"
msgstr "Area con vento"

#: libgweather/gweather-weather.c:267
msgid "Drifting sand"
msgstr "Area flutuante"

#. HAZE
#: libgweather/gweather-weather.c:268
msgid "Haze"
msgstr "Bruma"

#. SPRAY
#: libgweather/gweather-weather.c:269
msgid "Blowing sprays"
msgstr "Orballo con vento"

#. DUST
#: libgweather/gweather-weather.c:270
msgid "Dust"
msgstr "Po"

#: libgweather/gweather-weather.c:270
msgid "Blowing dust"
msgstr "Po con vento"

#: libgweather/gweather-weather.c:270
msgid "Drifting dust"
msgstr "Po flutuante"

#. SQUALL
#: libgweather/gweather-weather.c:271
msgid "Squall"
msgstr "Refachos de vento"

#. SANDSTORM
#: libgweather/gweather-weather.c:272
msgid "Sandstorm"
msgstr "Treboada de area"

#: libgweather/gweather-weather.c:272
msgid "Sandstorm in the vicinity"
msgstr "Treboada de area nas proximidades"

#: libgweather/gweather-weather.c:272
msgid "Heavy sandstorm"
msgstr "Treboada forte de area"

#. DUSTSTORM
#: libgweather/gweather-weather.c:273
msgid "Duststorm"
msgstr "Treboada de po"

#: libgweather/gweather-weather.c:273
msgid "Duststorm in the vicinity"
msgstr "Treboada de po nas proximidades"

#: libgweather/gweather-weather.c:273
msgid "Heavy duststorm"
msgstr "Treboada forte de po"

#. FUNNEL_CLOUD
#: libgweather/gweather-weather.c:274
msgid "Funnel cloud"
msgstr "Nube de funil"

#. TORNADO
#: libgweather/gweather-weather.c:275
msgid "Tornado"
msgstr "Tornado"

#. DUST_WHIRLS
#: libgweather/gweather-weather.c:276
msgid "Dust whirls"
msgstr "Remuíños de po"

#: libgweather/gweather-weather.c:276
msgid "Dust whirls in the vicinity"
msgstr "Remuíños de po nas proximidades"

#: libgweather/gweather-weather.c:799
msgid "%a, %b %d / %H∶%M"
msgstr "%a, %d de %b / %H:%M"

#: libgweather/gweather-weather.c:805
msgid "Unknown observation time"
msgstr "Hora da observación descoñecida"

#: libgweather/gweather-weather.c:817
msgctxt "sky conditions"
msgid "Unknown"
msgstr "Descoñecida"

#. Translate to the default units to use for presenting
#. * lengths to the user. Translate to default:inch if you
#. * want inches, otherwise translate to default:mm.
#. * Do *not* translate it to "predefinito:mm", if it
#. * it isn't default:mm or default:inch it will not work
#.
#: libgweather/gweather-weather.c:839
msgid "default:mm"
msgstr "default:mm"

#. TRANSLATOR: This is the temperature in degrees Fahrenheit (U+2109 DEGREE FAHRENHEIT)
#. * with a non-break space (U+00A0) between the digits and the degrees sign
#: libgweather/gweather-weather.c:892
#, c-format
msgid "%.1f ℉"
msgstr "%.1f ℉"

#. TRANSLATOR: This is the temperature in degrees Fahrenheit (U+2109 DEGREE FAHRENHEIT)i
#. * with a non-break space (U+00A0) between the digits and the degrees sign
#: libgweather/gweather-weather.c:896
#, c-format
msgid "%d ℉"
msgstr "%d ℉"

#. TRANSLATOR: This is the temperature in degrees Celsius (U+2103 DEGREE CELSIUS)
#. * with a non-break space (U+00A0) between the digits and the degrees sign
#: libgweather/gweather-weather.c:903
#, c-format
msgid "%.1f ℃"
msgstr "%.1f ℃"

#. TRANSLATOR: This is the temperature in degrees Celsius (U+2103 DEGREE CELSIUS)
#. * with a non-break space (U+00A0) between the digits and the degrees sign
#: libgweather/gweather-weather.c:907
#, c-format
msgid "%d ℃"
msgstr "%d ℃"

#. TRANSLATOR: This is the temperature in kelvin (U+212A KELVIN SIGN)
#. * with a non-break space (U+00A0) between the digits and the degrees sign
#: libgweather/gweather-weather.c:914
#, c-format
msgid "%.1f K"
msgstr "%.1f K"

#. TRANSLATOR: This is the temperature in kelvin (U+212A KELVIN SIGN)
#. * with a non-break space (U+00A0) between the digits and the degrees sign
#: libgweather/gweather-weather.c:918
#, c-format
msgid "%d K"
msgstr "%d K"

#: libgweather/gweather-weather.c:941 libgweather/gweather-weather.c:957
#: libgweather/gweather-weather.c:973 libgweather/gweather-weather.c:1035
msgctxt "temperature"
msgid "Unknown"
msgstr "Descoñecida"

#: libgweather/gweather-weather.c:995
msgctxt "dew"
msgid "Unknown"
msgstr "Descoñecidof"

#: libgweather/gweather-weather.c:1015
msgctxt "humidity"
msgid "Unknown"
msgstr "Descoñecida"

#. TRANSLATOR: This is the humidity in percent
#: libgweather/gweather-weather.c:1018
#, c-format
msgid "%.f%%"
msgstr "%.f%%"

#. TRANSLATOR: This is the wind speed in knots
#: libgweather/gweather-weather.c:1064
#, c-format
msgid "%0.1f knots"
msgstr "%0.1f nós"

#. TRANSLATOR: This is the wind speed in miles per hour
#: libgweather/gweather-weather.c:1067
#, c-format
msgid "%.1f mph"
msgstr "%.1f mph"

#. TRANSLATOR: This is the wind speed in kilometers per hour
#: libgweather/gweather-weather.c:1070
#, c-format
msgid "%.1f km/h"
msgstr "%.1f km/h"

#. TRANSLATOR: This is the wind speed in meters per second
#: libgweather/gweather-weather.c:1073
#, c-format
msgid "%.1f m/s"
msgstr "%.1f m/s"

#. TRANSLATOR: This is the wind speed as a Beaufort force factor
#. * (commonly used in nautical wind estimation).
#.
#: libgweather/gweather-weather.c:1078
#, c-format
msgid "Beaufort force %.1f"
msgstr "Forza Beaufort %.1f"

#: libgweather/gweather-weather.c:1099
msgctxt "wind speed"
msgid "Unknown"
msgstr "Descoñecida"

#: libgweather/gweather-weather.c:1101
msgid "Calm"
msgstr "Calma"

#. TRANSLATOR: This is 'wind direction' / 'wind speed'
#: libgweather/gweather-weather.c:1109
#, c-format
msgid "%s / %s"
msgstr "%s / %s"

#: libgweather/gweather-weather.c:1145
msgctxt "pressure"
msgid "Unknown"
msgstr "Descoñecida"

#. TRANSLATOR: This is pressure in inches of mercury
#: libgweather/gweather-weather.c:1151
#, c-format
msgid "%.2f inHg"
msgstr "%.2f inHg"

#. TRANSLATOR: This is pressure in millimeters of mercury
#: libgweather/gweather-weather.c:1154
#, c-format
msgid "%.1f mmHg"
msgstr "%.1f mmHg"

#. TRANSLATOR: This is pressure in kiloPascals
#: libgweather/gweather-weather.c:1157
#, c-format
msgid "%.2f kPa"
msgstr "%.2f kPa"

#. TRANSLATOR: This is pressure in hectoPascals
#: libgweather/gweather-weather.c:1160
#, c-format
msgid "%.2f hPa"
msgstr "%.2f hPa"

#. TRANSLATOR: This is pressure in millibars
#: libgweather/gweather-weather.c:1163
#, c-format
msgid "%.2f mb"
msgstr "%.2f mb"

#. TRANSLATOR: This is pressure in atmospheres
#: libgweather/gweather-weather.c:1166
#, c-format
msgid "%.3f atm"
msgstr "%.3f atm"

#: libgweather/gweather-weather.c:1204
msgctxt "visibility"
msgid "Unknown"
msgstr "Descoñecida"

#. TRANSLATOR: This is the visibility in miles
#: libgweather/gweather-weather.c:1210
#, c-format
msgid "%.1f miles"
msgstr "%.1f millas"

#. TRANSLATOR: This is the visibility in kilometers
#: libgweather/gweather-weather.c:1213
#, c-format
msgid "%.1f km"
msgstr "%.1f km"

#. TRANSLATOR: This is the visibility in meters
#: libgweather/gweather-weather.c:1216
#, c-format
msgid "%.0fm"
msgstr "%.0fm"

#: libgweather/gweather-weather.c:1244 libgweather/gweather-weather.c:1269
msgid "%H∶%M"
msgstr "%H∶%M"

#: libgweather/gweather-weather.c:1361
msgid "Retrieval failed"
msgstr "Produciuse un fallou na recuperación"

#. Translators: %d is an error code, and %s the error string
#: libgweather/weather-metar.c:599
#, c-format
msgid "Failed to get METAR data: %d %s.\n"
msgstr "Produciuse un erro ao obter os datos de METAR: %d %s.\n"

#: libgweather/weather-owm.c:383
msgid ""
"Weather data from the <a href=\"https://openweathermap.org\">Open Weather "
"Map project</a>"
msgstr ""
"Datos do tempo de <a href=\"https://openweathermap.org\">Proxecto aberto de "
"mapas do tempo</a>"

#. The new (documented but not advertised) API is less strict in the
#. format of the attribution, and just requires a generic CC-BY compatible
#. attribution with a link to their service.
#.
#. That's very nice of them!
#.
#: libgweather/weather-yrno.c:379
msgid ""
"Weather data from the <a href=\"https://www.met.no/\">Norwegian "
"Meteorological Institute</a>"
msgstr ""
"Datos do tempo de <a href=\"https://www.met.no/\">Instituto meteorolóxico "
"noruego</a>"

#: schemas/org.gnome.GWeather.gschema.xml:5
msgid "URL for the radar map"
msgstr "URL para o mapa do radar"

#: schemas/org.gnome.GWeather.gschema.xml:6
msgid ""
"The custom URL from where to retrieve a radar map, or empty for disabling "
"radar maps."
msgstr ""
"O URL personalizado desde onde obter un mapa de radar, o baleiro para "
"desactivar os mapas de radares."

#: schemas/org.gnome.GWeather.gschema.xml:13
msgid "Temperature unit"
msgstr "Unidade de temperatura"

#: schemas/org.gnome.GWeather.gschema.xml:14
msgid ""
"The unit of temperature used for showing weather. Valid values are “kelvin”, "
"“centigrade” and “fahrenheit”."
msgstr ""
"A unidade de temperatura usada para mostrar o tempo. Os valores posíbeis son "
"«kelvin», «centigrade» ou «fahrenheit»."

#: schemas/org.gnome.GWeather.gschema.xml:21
msgid "Distance unit"
msgstr "Unidade de distancia"

#: schemas/org.gnome.GWeather.gschema.xml:22
msgid ""
"The unit of distance used for showing weather (for example for visibility or "
"for distance of important events). Valid values are “meters”, “km” and "
"“miles”."
msgstr ""
"A unidade de distancia usada para mostrar o tempo (por exemplo, para a "
"visibilidade ou para a distancia de eventos importantes). Os valores "
"posíbeis son «meters», «km» e «miles»."

#: schemas/org.gnome.GWeather.gschema.xml:29
msgid "Speed unit"
msgstr "Unidade de velocidade"

#: schemas/org.gnome.GWeather.gschema.xml:30
msgid ""
"The unit of speed used for showing weather (for example for wind speed). "
"Valid values are “ms” (meters per second), “kph” (kilometers per hour), "
"“mph” (miles per hour), “knots” and “bft” (Beaufort scale)."
msgstr ""
"A unidade de velocidade usada para mostrar o tempo (por exemplo, para a "
"velocidade do vento). Os valores posíbeis son «ms» (metros por segundo), "
"«kph» (kilómetros por hora), «mph» (millas por hora), «nudos» e "
"«bft» (escala Beaufort)."

#: schemas/org.gnome.GWeather.gschema.xml:38
msgid "Pressure unit"
msgstr "Unidade de presión"

#: schemas/org.gnome.GWeather.gschema.xml:39
msgid ""
"The unit of pressure used for showing weather. Valid values are "
"“kpa” (kilopascal), “hpa” (hectopascal), “mb” (millibar, mathematically "
"equivalent to 1 hPa but shown differently), “mm-hg” (millimeters of "
"mercury), “inch-hg” (inches of mercury), “atm” (atmospheres)."
msgstr ""
"A unidade de presión usada para mostrar o tempo. Os valores posíbeis son "
"kpa» (kilopascais), «hpa» (hectopascais) , «mb» (milibares, matematicamente "
"equivalentes a 1 hpa, pero mostrados de diferente maneira), «mm-"
"hg» (milímetros de mercurio) e «inch-hg» (polgadas de mercurio), "
"«atm» (atmosferas)."

#. TRANSLATORS: pick a default location to use in the weather applet. This should
#. usually be the largest city or the capital of your country. If you're not picking
#. a <location> in the database, don't forget to set name and coordinates.
#. Do NOT change or localize the quotation marks!
#: schemas/org.gnome.GWeather.gschema.xml:51
msgid "('', 'KNYC', nothing)"
msgstr "('', 'LEST', nothing)"

#: schemas/org.gnome.GWeather.gschema.xml:52
msgid "Default location"
msgstr "Localización predeterminada"

#: schemas/org.gnome.GWeather.gschema.xml:53
msgid ""
"The default location for the weather applet. The first field is the name "
"that will be shown. If empty, it will be taken from the locations database. "
"The second field is the METAR code for the default weather station. It must "
"not be empty and must correspond to a <code> tag in the Locations.xml file. "
"The third field is a tuple of (latitude, longitude), to override the value "
"taken from the database. This is only used for sunrise and moon phase "
"calculations, not for weather forecast."
msgstr ""
"A localización predeterminada para a miniaplicación do clima. O primeiro "
"campo é o nome que se mostrará. Se está baleiro, tomarase da base de datos "
"de localizacións. O segundo campo é o código METAR para a estación do clima "
"predeterminada. Non debe estar baleiro e debe corresponder coa etiqueta "
"<code> no ficheiro Locations.xml. O terceiro campo é una tupla de (latitude, "
"longitude), para sobrescribir os valores obtidos da base de datos. Isto só "
"se usa para calculas as fases da lúa, non para a previsión do tempo."
